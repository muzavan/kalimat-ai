﻿using kalimat.ai.business.Model;
using kalimat.ai.business.Store;
using kalimat.ai.business.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace kalimat.ai.web.Controllers.Api
{
    public class ConversationApiController : ApiController
    {

        /// <summary>
        /// Question 1.1 : Why the type of store is IStore and not Memstore?
        /// Answer : By design, we put IStore as the parameter in Manager (Repo) Constructor. We design this to make the Manager compatible with any IStore implementation (whether it's stored in Memory or Database). 
        /// By doing this, we can change our data store by changing the initialization of IStore and don't need to update or add implementation in Manager.
        /// 
        /// Question 1.2 : What design patterns do you recognize here?
        /// Answer : Based on the case and the design, I recognize this pattern as Dependency Injection (as defined in here: https://en.wikipedia.org/wiki/Dependency_injection), because, to quote wiki, we implement 
        /// 1. "... technique whereby one object supplies the dependencies of another object..."  ==> in UserManager and ConversationManager constructor
        /// 2. "... The service is made part of the client's state ..." ==> as we can see IStore is a part of properties in UserManager and ConversationManager
        /// 3. "... Passing the service to the client, rather than allowing a client to build or find the service ..." ==> As we only assign IStore to our manager without Modifying it (we assume IStore building/initialization already done before passed as parameter)
        /// 
        /// </summary>
        private static IStore store = MemoryStore.GetInstance();
        private static ConversationManager manager = new ConversationManager(store);

        [HttpGet]
        [Route("conversations")]
        public List<Conversation> Get([FromUri] int page = 1, [FromUri] int limit = 5)
        {
            return manager.List(page, limit);
        }

        [HttpGet]
        [Route("conversations/{id:int}")]
        public Conversation Get(int id)
        {
            return manager.Get(id);
        }

        [HttpPost]
        [Route("conversations")]
        public Conversation Create([FromBody] Dictionary<string, string> data)
        {
            return manager.Create(data);
        }

        [HttpPut]
        [Route("conversations/{id:int}")]
        public Conversation Update(int id, [FromBody] Dictionary<string, string> data)
        {
            return manager.Update(id, data);
        }

        [HttpDelete]
        [Route("conversations/{id:int}")]
        public Conversation Delete(int id)
        {
            return manager.Remove(id);
        }
    }
}