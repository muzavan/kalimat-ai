﻿using kalimat.ai.business.Model;
using kalimat.ai.business.Store;
using kalimat.ai.business.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace kalimat.ai.web.Controllers.Api
{
    public class UserApiController : ApiController
    {
        private static IStore store = MemoryStore.GetInstance();
        private static UserManager manager = new UserManager(store);

        [HttpGet]
        [Route("users")]
        public List<User> Get([FromUri] int page = 1, [FromUri] int limit = 5)
        {
            return manager.List(page,limit);
        }

        [HttpGet]
        [Route("users/{id:int}")]
        public User Get(int id)
        {
            return manager.Get(id);
        }

        [HttpPost]
        [Route("users")]
        public User Create([FromBody] Dictionary<string,string> data)
        {
            return manager.Create(data);
        }

        [HttpPut]
        [Route("users/{id:int}")]
        public User Update(int id, [FromBody] Dictionary<string, string> data)
        {
            return manager.Update(id,data);
        }

        [HttpDelete]
        [Route("users/{id:int}")]
        public User Delete(int id)
        {
            return manager.Remove(id);
        }
    }
}