﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kalimat.ai.business.Manager;
using kalimat.ai.business.Store;
using kalimat.ai.business.Model;
using kalimat.ai.business.Util;
using System.Collections.Generic;

namespace Task1UnitTest
{
    /// <summary>
    /// Test Unit : All classes in kalimat.ai.business.Manager namespace, except the abstract ones, because Store is used in manager, Store is actually also tested in Manager
    /// </summary>
    [TestClass]
    public class ManagerTest
    {
        /// <summary>
        /// Test Case : CRUD for UserManager
        /// </summary>
        [TestMethod]
        public void UserManagerTest()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary[UserConstant.NAME_KEY] = "Name";
            dictionary[UserConstant.PHONE_KEY] = "0123456789";
            dictionary[UserConstant.GENDER_KEY] = "male";
            dictionary[UserConstant.CITY_KEY] = "city";
            dictionary[UserConstant.EMAIL_KEY] = "email@email.com";

            IStore store = MemoryStore.GetInstance();
            var manager = new UserManager(store);

            // Create
            var createdUser = manager.Create(dictionary);
            Assert.AreEqual(createdUser.ID,0);
            Assert.AreEqual(createdUser.Name, "Name");
            Assert.AreEqual(createdUser.Phone, "0123456789");
            Assert.AreEqual(createdUser.Gender, User.GenderEnum.MALE);
            Assert.AreEqual(createdUser.City, "city");
            Assert.AreEqual(createdUser.Email, "email@email.com");

            // Read
            var gotUser = manager.Get(0);
            Assert.AreEqual(createdUser, gotUser);
            var gotUser2 = manager.Get(1);
            Assert.AreEqual(gotUser2, default(User));

            // Update
            var updateDictionary = new Dictionary<string, string>();
            updateDictionary[UserConstant.GENDER_KEY] = "female";

            var updatedUser = manager.Update(0, updateDictionary);
            Assert.AreNotEqual(updatedUser.Gender, User.GenderEnum.MALE);

            // Delete
            var deletedUser = manager.Remove(0);
            Assert.AreEqual(updatedUser, deletedUser);

            var deletedUser2 = manager.Remove(0);
            Assert.AreEqual(deletedUser2, default(User));
        }

        /// <summary>
        /// Test Case : CRUD for ConversationManager
        /// </summary>
        [TestMethod]
        public void ConversationManagerTest()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary[ConversationConstant.USERID_KEY] = "0";
            dictionary[ConversationConstant.DIRECTION_KEY] = "incoming";
            dictionary[ConversationConstant.MESSAGE_KEY] = "message";
            dictionary[ConversationConstant.TIMESTAMP_KEY] = "20000";

            IStore store = MemoryStore.GetInstance();
            var manager = new ConversationManager(store);

            // Create
            var createdConversation = manager.Create(dictionary);
            Assert.AreEqual(createdConversation.ID, 0);
            Assert.AreEqual(createdConversation.UserID, "0");
            Assert.AreEqual(createdConversation.Direction, Conversation.DirectionEnum.INCOMING);
            Assert.AreEqual(createdConversation.Message, "message");
            Assert.AreEqual(createdConversation.Timestamp, 20000);

            // Read
            var gotConversation = manager.Get(0);
            Assert.AreEqual(createdConversation, gotConversation);
            var gotConversation2 = manager.Get(1);
            Assert.AreEqual(gotConversation2, default(Conversation));

            // Update
            var updateDictionary = new Dictionary<string, string>();
            updateDictionary[ConversationConstant.DIRECTION_KEY] = "anything";

            var updatedConversation = manager.Update(0, updateDictionary);
            Assert.AreNotEqual(updatedConversation.Direction, Conversation.DirectionEnum.INCOMING);

            // Delete
            var deletedConversation = manager.Remove(0);
            Assert.AreEqual(updatedConversation, deletedConversation);

            var deletedConversation2 = manager.Remove(0);
            Assert.AreEqual(deletedConversation2, default(Conversation));
        }
    }
}
