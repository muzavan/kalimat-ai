﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using kalimat.ai.business.Model;
using System;

namespace Task1UnitTest
{
    /// <summary>
    /// Test Unit : All classes in kalimat.ai.business.Model namespace, except the abstract ones
    /// </summary>
    [TestClass]
    public class ModelTest
    {
        /// <summary>
        /// Test Case : Constructor and public method testing for User
        /// </summary>
        [TestMethod]
        public void ConversationTest()
        {
            var conversation = new Conversation();
            Assert.AreNotEqual(conversation, null);
            conversation.ID = 1;
            Assert.AreEqual(conversation.ID, 1);
            conversation.UserID = "UserID";
            Assert.AreEqual(conversation.UserID, "UserID");
            conversation.Message = "Message";
            Assert.AreEqual(conversation.Message, "Message");
            conversation.Direction = Conversation.DirectionEnum.INCOMING;
            Assert.AreEqual(conversation.Direction, Conversation.DirectionEnum.INCOMING);

            var timestamp = 9999999999999;
            try
            {
                conversation.Timestamp = (int)timestamp;
                Assert.Fail("Timestamp should be long only");
            }
            catch(Exception ex)
            {
                int iTimestamp = 9999;
                conversation.Timestamp = iTimestamp;
                Assert.AreEqual(conversation.Timestamp, iTimestamp);
            }
            
            

        }

        /// <summary>
        /// Test Case : Constructor and public method testing for User
        /// </summary>
        [TestMethod]
        public void UserTest()
        {
            var user = new User();
            Assert.AreNotEqual(user, null);
            user.ID = 1;
            Assert.AreEqual(user.ID, 1);
            user.Name = "name";
            Assert.AreEqual(user.Name, "name");
            user.Gender = User.GenderEnum.FEMALE;
            Assert.AreEqual(user.Gender, User.GenderEnum.FEMALE);
            user.City = "city";
            Assert.AreEqual(user.City, "city");
            user.Phone = "0982733";
            Assert.AreEqual(user.Phone, "0982733");
            try
            {
                user.Email = "thisisemail";
                Assert.Fail("Email should have '@'");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(ex.Message,"Bad Format For Email");
            }
            
        }
    }
}
