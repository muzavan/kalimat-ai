﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalimat.ai.business.Util
{
    public class AppConstant
    {

    }

    public class UserConstant
    {
        public static string NAME_KEY = "name";
        public static string GENDER_KEY = "gender";
        public static string EMAIL_KEY = "email";
        public static string PHONE_KEY = "phone";
        public static string CITY_KEY = "city";

        public static string MALE_COMP = "male";
        public static string MAIL_DEFAULT = "mail@mail.com";
    }

    public class ConversationConstant
    {
        public static string USERID_KEY = "userId";
        public static string DIRECTION_KEY = "direction";
        public static string MESSAGE_KEY = "message";
        public static string TIMESTAMP_KEY = "timestamp";

        public static string INCOMING_COMP = "incoming";
    }

}
