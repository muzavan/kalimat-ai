﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalimat.ai.business.Model
{
    public abstract class BaseModel
    {
        #region Protected Property
        protected int _id;
        #endregion

        #region Public Method
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        #endregion
    }
}
