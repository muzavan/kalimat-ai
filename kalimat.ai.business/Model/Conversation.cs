﻿
namespace kalimat.ai.business.Model
{
    public class Conversation:BaseModel
    {
        #region Private Property
        private string _userId;
        private DirectionEnum _direction;
        private string _message;
        private int _timestamp;

        #endregion

        #region Public Method
        public string UserID
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public DirectionEnum Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        public int Timestamp
        {
            get
            {
                return _timestamp;
            }
            set
            {
                _timestamp = value;
            }
        }
        #endregion

        #region Enum
        public enum DirectionEnum
        {
            INCOMING, OUTGOING
        };
        #endregion



    }
}
