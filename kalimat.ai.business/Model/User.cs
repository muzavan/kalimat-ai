﻿using System;

namespace kalimat.ai.business.Model
{
    public class User:BaseModel
    {
        #region Private Property
        private string _name;
        private GenderEnum _gender;
        private string _city;
        private string _phone;
        private string _email;
        #endregion

        #region Public Method
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public GenderEnum Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (value.Contains("@"))
                {
                    _email = value;
                }
                else
                {
                    throw new Exception("Bad Format For Email");
                }
                
            }
        }

        #endregion

        #region Enum
        public enum GenderEnum
        {
            MALE, FEMALE
        }
        #endregion
    }
}
