﻿using System;
using System.Collections.Generic;
using kalimat.ai.business.Store;
using kalimat.ai.business.Model;
using kalimat.ai.business.Util;

namespace kalimat.ai.business.Manager
{
    public class ConversationManager
    {
        #region Private Property
        private IStore _store;
        #endregion

        #region Public Method
        public ConversationManager(IStore store)
        {
            _store = store;
        }

        public Conversation Get(int id)
        {
            return _store.Get<Conversation>(id);
        }

        public List<Conversation> List(int page, int limit)
        {
            return _store.GetList<Conversation>(page, limit);
        }

        public Conversation Create(Dictionary<string, string> data)
        {
            var conversation = new Conversation();
            var userId = "";
            var direction = "";
            var message = "";
            var timestamp = "";

            if (data.ContainsKey(ConversationConstant.USERID_KEY))
            {
                conversation.UserID = data[ConversationConstant.USERID_KEY];
            }
            if (data.ContainsKey(ConversationConstant.DIRECTION_KEY))
            {
                conversation.Direction = data[ConversationConstant.DIRECTION_KEY].ToLower() == ConversationConstant.INCOMING_COMP ? Conversation.DirectionEnum.INCOMING : Conversation.DirectionEnum.OUTGOING;
            }
            if (data.ContainsKey(ConversationConstant.MESSAGE_KEY))
            {
                conversation.Message = data[ConversationConstant.MESSAGE_KEY];
            }
            if (data.ContainsKey(ConversationConstant.TIMESTAMP_KEY))
            {
                conversation.Timestamp = Convert.ToInt32(data[ConversationConstant.TIMESTAMP_KEY]);
            }

            return _store.Create<Conversation>(conversation);
        }

        public Conversation Update(int id, Dictionary<string, string> data)
        {
            var conversation = _store.Get<Conversation>(id);
            if (conversation == null)
            {
                return null;
            }
            if (data.ContainsKey(ConversationConstant.USERID_KEY))
            {
                conversation.UserID = data[ConversationConstant.USERID_KEY];
            }
            if (data.ContainsKey(ConversationConstant.DIRECTION_KEY))
            {
                conversation.Direction = data[ConversationConstant.DIRECTION_KEY].ToLower() == ConversationConstant.INCOMING_COMP ? Conversation.DirectionEnum.INCOMING : Conversation.DirectionEnum.OUTGOING;
            }
            if (data.ContainsKey(ConversationConstant.MESSAGE_KEY))
            {
                conversation.Message = data[ConversationConstant.MESSAGE_KEY];
            }
            if (data.ContainsKey(ConversationConstant.TIMESTAMP_KEY))
            {
                conversation.Timestamp = Convert.ToInt32(data[ConversationConstant.TIMESTAMP_KEY]);
            }

            return _store.Update<Conversation>(id, conversation);
        }

        public Conversation Remove(int id)
        {
            return _store.Remove<Conversation>(id);
        }

        #endregion
    }
}
