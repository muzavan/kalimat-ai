﻿using kalimat.ai.business.Store;
using kalimat.ai.business.Model;
using System.Collections.Generic;
using System;
using kalimat.ai.business.Util;

namespace kalimat.ai.business.Manager
{
    public class UserManager
    {
        #region Private Property
        private IStore _store;
        #endregion

        #region Public Method
        public UserManager(IStore store)
        {
            _store = store;
        }

        public User Get(int id)
        {
            return _store.Get<User>(id);
        }

        public List<User> List(int page, int limit)
        {
            return _store.GetList<User>(page,limit);
        }

        public User Create(Dictionary<string, string> data)
        {
            var user = new User();
            var name = "";
            var city = "";
            var phone = "";
            var email = "";
            var gender = "";

            if (data.ContainsKey(UserConstant.NAME_KEY))
            {
                user.Name = data[UserConstant.NAME_KEY];
            }
            if (data.ContainsKey(UserConstant.GENDER_KEY))
            {
                user.Gender = data[UserConstant.GENDER_KEY].ToLower() == UserConstant.MALE_COMP ? User.GenderEnum.MALE : User.GenderEnum.FEMALE;
            }
            if (data.ContainsKey(UserConstant.CITY_KEY))
            {
                user.City = data[UserConstant.CITY_KEY];
            }
            if (data.ContainsKey(UserConstant.PHONE_KEY))
            {
                user.Phone = data[UserConstant.PHONE_KEY];
            }
            if (data.ContainsKey(UserConstant.EMAIL_KEY))
            {
                user.Email = data[UserConstant.EMAIL_KEY];
            }

            return _store.Create<User>(user);
        }

        public User Update(int id, Dictionary<string, string> data)
        {
            var user = _store.Get<User>(id);
            if (user == null)
            {
                return null;
            }
            if (data.ContainsKey(UserConstant.NAME_KEY))
            {
                user.Name = data[UserConstant.NAME_KEY];
            }
            if (data.ContainsKey(UserConstant.GENDER_KEY))
            {
                user.Gender = data[UserConstant.GENDER_KEY].ToLower() == UserConstant.MALE_COMP ? User.GenderEnum.MALE : User.GenderEnum.FEMALE;
            }
            if (data.ContainsKey(UserConstant.CITY_KEY))
            {
                user.City = data[UserConstant.CITY_KEY];
            }
            if (data.ContainsKey(UserConstant.PHONE_KEY))
            {
                user.Phone = data[UserConstant.PHONE_KEY];
            }
            if (data.ContainsKey(UserConstant.EMAIL_KEY))
            {
                user.Email = data[UserConstant.EMAIL_KEY];
            }

            return _store.Update<User>(id, user);
        }

        public User Remove(int id)
        {
            return _store.Remove<User>(id);
        }

        #endregion
    }
}
