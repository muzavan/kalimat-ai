﻿using kalimat.ai.business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalimat.ai.business.Store
{
    public class MemoryStore : IStore
    {
        #region Private Property
        private static MemoryStore _memoryStore; // will be created as singleton so the store consistent across different class
        private Dictionary<Type, Dictionary<int,object>> _memory;
        #endregion

        #region Public Method
        public static MemoryStore GetInstance()
        {
            if(_memoryStore == null)
            {
                _memoryStore = new MemoryStore();
            }

            return _memoryStore;
        }

        public T Create<T>(T obj) where T : BaseModel
        {
            var type = typeof(T);
            CreateDictionary(type);
            var id = _memory[type].Keys.Count;
            try
            {
                _memory[type][id] = obj;
                obj.ID = id;
            }
            catch(Exception)
            {
                return default(T);
            }
            return obj;
        }

        public T Get<T>(int id) where T : BaseModel
        {
            var type = typeof(T);
            if (_memory.ContainsKey(type))
            {
                T retrieved = default(T); // object T that will be returned
                object retrievedObject = null;

                _memory[type].TryGetValue(id,out retrievedObject); // try to get value of id, if id doesnt exist, obj will stay null
                if (retrievedObject != null)
                {
                    retrieved = retrievedObject as T; // casting object as T
                }
                return retrieved;
            }
            return default(T);
        }

        public List<T> GetList<T>(int page, int limit) where T : BaseModel
        {
            var list = new List<T>();
            var type = typeof(T);

            if (_memory.ContainsKey(type))
            {
                var memoryKeys = _memory[type].Keys.ToArray();
                var offset = (page - 1) * limit;

                // because idx is 0-indexed and offset is start in 1, the start idx should be offset
                for (var idx = offset; idx < memoryKeys.Length && list.Count < limit; idx++)
                {
                    var storedT = _memory[type][memoryKeys[idx]] as T;
                    list.Add(storedT);
                }
            }

            return list;
        }

        public T Remove<T>(int id) where T : BaseModel
        {
            T removed = default(T);
            var type = typeof(T);
            if (_memory.ContainsKey(type))
            {
                if (_memory[type].ContainsKey(id))
                {
                    removed = _memory[type][id] as T;
                    _memory[type].Remove(id);
                }
            }
            return removed;
            
        }

        public T Update<T>(int id, T newObj) where T : BaseModel
        {
            var type = typeof(T);
            if (_memory.ContainsKey(type))
            {
                if (_memory[type].ContainsKey(id))
                {
                    _memory[type][id] = newObj;
                    return newObj;
                }
            }
            return default(T);
        }

        #endregion

        #region Private Method
        private MemoryStore(): base()
        {
            _memory = new Dictionary<Type, Dictionary<int, object>>();
        }

        private void CreateDictionary(Type type)
        {
            if (!_memory.ContainsKey(type))
            {
                _memory[type] = new Dictionary<int, object>();
            }
        }
        #endregion
    }
}
