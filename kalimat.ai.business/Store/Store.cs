﻿using System.Collections.Generic;
using kalimat.ai.business.Model;

namespace kalimat.ai.business.Store
{
    public interface IStore
    {
        T Get<T>(int id) where T : BaseModel ;
        List<T> GetList<T>(int page, int limit) where T : BaseModel;
        T Create<T>(T obj) where T : BaseModel;
        T Update<T>(int id, T newObj) where T : BaseModel;
        T Remove<T>(int id) where T : BaseModel;
    }
}
